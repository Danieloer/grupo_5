from django.db import models
from .user import User

class Account(models.Model):
  id = models.AutoField(primary_key=True)
  user = models.ForeignKey(User, related_name='account', on_delete=models.CASCADE)
  balance = models.IntegerField(default=0)
  profile_photo = models.CharField ("Foto de perfil",max_length = 100,default=" ")
  id_photo = models.CharField ("Foto de ID",max_length = 100,default=" ")
  license_photo = models.CharField ("Foto de la licencia",max_length = 100,default=" ")
  experience = models.CharField ("Experiencia",max_length = 100,default=" ")
  lastChangeDate = models.DateTimeField()
  isActive = models.BooleanField(default=True)

