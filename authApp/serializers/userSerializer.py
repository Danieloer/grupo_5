from rest_framework import serializers
from authApp.models.user import User
from authApp.models.account import Account
from authApp.serializers.accountSerializer import AccountSerializer

class UserSerializer(serializers.ModelSerializer):
    account = AccountSerializer()
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'email','city','vehicule','type_of_cargo','account']

    def create(self, validated_data):
        accountData = validated_data.pop('account')
        userInstance = User.objects.create(**validated_data)
        Account.objects.create(user=userInstance, **accountData)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        account = Account.objects.get(user=obj.id)
        return {
            'id': user.id,
            'username': user.username,
            'name': user.name,
            'email': user.email,
            'city': user.city,
            'vehicule': user.vehicule,
            'type_of_cargo': user.type_of_cargo,
            'account': {
                'id': account.id,
                'balance':account.balance,
                'profile_photo':account.profile_photo,
                'id_photo': account.id_photo,
                'license_photo': account.license_photo,
                'experience': account.experience,
                'lastChangeDate': account.lastChangeDate,
                'isActive': account.isActive
            },
        }