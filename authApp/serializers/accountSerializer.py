from authApp.models.account import Account
from rest_framework import serializers
class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['profile_photo', 'id_photo', 'license_photo', 'experience','lastChangeDate','isActive']